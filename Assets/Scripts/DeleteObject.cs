﻿using System.Collections;
using System.Collections.Generic;
using GoogleARCore.HelloAR;
using UnityEngine;

public class DeleteObject : MonoBehaviour
{
    ARController c;

    public void Start()
    {
        GameObject controller = GameObject.FindGameObjectWithTag("Controller");
        c = controller.GetComponent<ARController>();
    }

    // Delete object function
    public void Delete_Object()
    {
		Destroy(c.publicHit.collider.gameObject.transform.root.gameObject);// (c.mainObject);
                
        StartCoroutine(ContinueDeletion());

    }

	IEnumerator ContinueDeletion()
    {
		yield return new WaitForEndOfFrame();
		GameObject[] objects = GameObject.FindGameObjectsWithTag("CenterObject");
        if (objects.Length == 0)
        {
			c.mainObjectPlaced = false;
			c.TrackedPlanePrefab.GetComponent<TrackedPlaneVisualizer>().OnTogglePlanes(true);
        }

		c.bottomBar.SetActive(false);

		yield return null;
    }
}
