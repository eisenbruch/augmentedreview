﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainObjectIndex : MonoBehaviour {

    public int index;
    public bool comingFromReviewSession;

	public void Awake()
	{
        DontDestroyOnLoad(this);
	}

	public void SetMainObjectIndex(int i)
    {
        index = i;
    }
}
