﻿using System.Collections;
using System.Collections.Generic;
using GoogleARCore.HelloAR;
using UnityEngine;

public class AddObjectToSceneController : MonoBehaviour {
   
	public GameObject controller;
	public ARController c;

	public GameObject selectObjectPanel;



	public void Start()
	{
		selectObjectPanel.SetActive(false);
		c = controller.GetComponent<ARController>();
	}


	public void Add_Object()
	{
		//ARController c = controller.GetComponent<ARController>();
		c.GetComponent<ARController>().mainObjectPlaced = false;
        c.TrackedPlanePrefab.GetComponent<TrackedPlaneVisualizer>().OnTogglePlanes(true);
	}

	public void Set_Placeable_Object(int selectedMainObjectIndex)
	{
		StartCoroutine(LoadAsset(selectedMainObjectIndex)); 
		selectObjectPanel.SetActive(false);
	}


	public void Show_selectObjectPanel()
	{
		selectObjectPanel.SetActive(true);
	}
    
	IEnumerator LoadAsset(int selectedMainObjectIndex)
    {
        ResourceRequest resourceRequest = Resources.LoadAsync<GameObject>(c.mainObjectPrefabsArray[selectedMainObjectIndex]);
        while (!resourceRequest.isDone)
        {
            yield return 0;
        }
        c.mainObjectPrefab = resourceRequest.asset as GameObject;
    }

}
