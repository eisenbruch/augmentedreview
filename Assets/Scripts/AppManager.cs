﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AppManager : MonoBehaviour {

    public GameObject mainConversationListView;
    public GameObject GroundDrone_Conversation_View;
    public GameObject GeoChair_Conversation_View;
	public GameObject GeoTable_Conversation_View;

    public GameObject NewReviewView;

    GameObject[] activePanels;
    GameObject[] reviewPanels;

    public void Awake()
    {
        DontDestroyOnLoad(this);
    }

    public void Start()
    {
        Close_All_Panels();
        switch (PlayerPrefs.GetString("Last_Conversation_Selected"))
        {
            case "":
                To_Review_List();
                break;
            case "ground drone":
                GroundDrone_ConversationSelected();
                break;
            case "geochair":
                GeoChair_ConversationSelected();
                break;
			case "geotable":
				GeoTable_ConversationSelected();
				break;            
        }
    }

    // User selects a review conversation, and we load that conversation
    //public void ChairConversationSelected()
    //{
    //    mainConversationListView.SetActive(false);
    //    chairConversationView.SetActive(true);
    //    PlayerPrefs.SetString("Last_Conversation_Selected", "chair");
        
    //}

    //// User selects a review conversation, and we load that conversation
    //public void Skp_4_ConversationSelected()
    //{
    //    mainConversationListView.SetActive(false);
    //    skp_4_ConversationView.SetActive(true);
    //    PlayerPrefs.SetString("Last_Conversation_Selected", "skp4");

    //}

    //public void Skp_2_ConversationSelected()
    //{
    //    mainConversationListView.SetActive(false);
    //    skp_2_ConversationView.SetActive(true);
    //    PlayerPrefs.SetString("Last_Conversation_Selected", "skp2");

    //}

    //public void Skp_3_ConversationSelected()
    //{
    //    mainConversationListView.SetActive(false);
    //    skp_3_ConversationView.SetActive(true);
    //    PlayerPrefs.SetString("Last_Conversation_Selected", "skp3");
    //}

    //public void Sofa_ConversationSelected()
    //{
    //    mainConversationListView.SetActive(false);
    //    Sofa_ConversationView.SetActive(true);
    //    PlayerPrefs.SetString("Last_Conversation_Selected", "sofa");
    //}

    //public void Bed_ConversationSelected()
    //{
    //    mainConversationListView.SetActive(false);
    //    Bed_ConversationView.SetActive(true);
    //    PlayerPrefs.SetString("Last_Conversation_Selected", "bed");
    //}

    public void GroundDrone_ConversationSelected()
    {
        mainConversationListView.SetActive(false);
        GroundDrone_Conversation_View.SetActive(true);
        PlayerPrefs.SetString("Last_Conversation_Selected", "ground drone");

    }

    public void GeoChair_ConversationSelected()
    {
        mainConversationListView.SetActive(false);
        GeoChair_Conversation_View.SetActive(true);
        PlayerPrefs.SetString("Last_Conversation_Selected", "geochair");

    }

	public void GeoTable_ConversationSelected()
    {
        mainConversationListView.SetActive(false);
        GeoTable_Conversation_View.SetActive(true);
        PlayerPrefs.SetString("Last_Conversation_Selected", "geotable");

    }

    public void To_Review_List()
    {
        mainConversationListView.SetActive(true);
        //chairConversationView.SetActive(false);
        //skp_4_ConversationView.SetActive(false);
        //skp_2_ConversationView.SetActive(false);
        //skp_3_ConversationView.SetActive(false);
        //Sofa_ConversationView.SetActive(false);
        //Bed_ConversationView.SetActive(false);
        GroundDrone_Conversation_View.SetActive(false);
        GeoChair_Conversation_View.SetActive(false);
		GeoTable_Conversation_View.SetActive(false);
        NewReviewView.SetActive(false);
        PlayerPrefs.SetString("Last_Conversation_Selected", "");
    }

    public void Close_All_Panels()
    {
        GameObject[] panels = GameObject.FindGameObjectsWithTag("Conversation View");
        foreach (GameObject panel in panels)
        {
            panel.SetActive(false);
        }
    }



    // User selects the chair model in the chair review, and we load the AR session with the chair model
    public void LoadARScene()
    {
        SceneManager.LoadSceneAsync("AR-Review-Scene");
    }

    public void BackToIntroScene()
    {
        SceneManager.LoadScene("Intro-UI-Scene");
    }

    public void NewReview()
    {
        mainConversationListView.SetActive(false);
        NewReviewView.SetActive(true);
    }

	private void OnApplicationPause(bool pause)
	{
        PlayerPrefs.DeleteAll();
	}


}
