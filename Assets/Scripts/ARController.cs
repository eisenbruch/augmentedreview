﻿namespace GoogleARCore.HelloAR
{
	using System.Collections;
	using System.Collections.Generic;
    using GoogleARCore;
    using UnityEngine;
    using UnityEngine.Rendering;
    using UnityEngine.UI;
    using UnityEngine.EventSystems;
    using Lean; 
    using System.Linq;

    public class ARController : MonoBehaviour
    {
        /// <summary>
        /// The first-person camera being used to render the passthrough camera image (i.e. AR background).
        /// </summary>
        public Camera FirstPersonCamera;
		public Camera ObjectCamera;

        /// <summary>
        /// A prefab for tracking and visualizing detected planes.
        /// </summary>
        public GameObject TrackedPlanePrefab;

        //public GameObject[] mainObjectPrefabs;
		public string[] mainObjectPrefabsArray = { "Ground Delivery Drone Prefab", "Geometric Chair Prefab", "Table Prefab", "WIRE_Ground Delivery Drone Prefab", "WIRE_Table Prefab" };
        public GameObject mainObjectPrefab;
        int selectedMainObjectIndex;

        /// <summary>
        /// A gameobject parenting UI for displaying the "searching for planes" snackbar.
        /// </summary>
        //public GameObject SearchingForPlaneUI;

        /// <summary>
        /// A list to hold new planes ARCore began tracking in the current frame. This object is used across
        /// the application to avoid per-frame allocations.
        /// </summary>
        private List<TrackedPlane> m_NewPlanes = new List<TrackedPlane>();

        /// <summary>
        /// A list to hold all planes ARCore is tracking in the current frame. This object is used across
        /// the application to avoid per-frame allocations.
        /// </summary>
        //private List<TrackedPlane> m_AllPlanes = new List<TrackedPlane>();

        /// <summary>
        /// A list to hold all planes ARCore is tracking in the current frame. This object is used across
        /// the application to avoid per-frame allocations.
        /// </summary>
        //private List<TrackedPlane> m_AllPlanes = new List<TrackedPlane>();

        /// <summary>
        /// True if the app is in the process of quitting due to an ARCore connection error, otherwise false.
        /// </summary>
        bool m_IsQuitting = false;

        public GameObject mainObject;

        public bool mainObjectPlaced = false;

        public GameObject clickedPointObject;

        public List<GameObject> clickedPoints = new List<GameObject>();

        public int selectedObjectInt;
        public GameObject selectedObject;

        public InputField inputField;

        public GameObject bottomBar;

		public RaycastHit publicHit;

		public void Awake()
		{
            selectedMainObjectIndex = GameObject.FindGameObjectWithTag("MainObjectIndex").GetComponent<MainObjectIndex>().index;
            Destroy(GameObject.FindGameObjectWithTag("MainObjectIndex"));
			StartCoroutine(LoadAsset());
		}

		public void Start()
        {
			bottomBar.SetActive(false);
        }

		IEnumerator LoadAsset ()
		{
			ResourceRequest resourceRequest = Resources.LoadAsync<GameObject>(mainObjectPrefabsArray[selectedMainObjectIndex]);
            while (!resourceRequest.isDone)
            {
                yield return 0;
            }
			mainObjectPrefab = resourceRequest.asset as GameObject;
		}

		public void Update()
        {
            if (Input.GetKey(KeyCode.Escape))
            {
                Application.Quit();
            }

            _QuitOnConnectionErrors();

            // Check that motion tracking is tracking.
            if (Session.Status != SessionStatus.Tracking)
            {
                const int lostTrackingSleepTimeout = 15;
                Screen.sleepTimeout = lostTrackingSleepTimeout;
                //if (!m_IsQuitting && Session.Status.IsValid())
                //{
                //    SearchingForPlaneUI.SetActive(true);
                //}

                return;
            }

            Screen.sleepTimeout = SleepTimeout.NeverSleep;

            // Iterate over planes found in this frame and instantiate corresponding GameObjects to visualize them.
            if (!mainObjectPlaced)
            {
                Vector3 planePosition = Vector3.zero;
                planePosition.y = 0.0001f;
                Session.GetTrackables<TrackedPlane>(m_NewPlanes, TrackableQueryFilter.New);
                for (int i = 0; i < m_NewPlanes.Count; i++)
                {
                    // Instantiate a plane visualization prefab and set it to track the new plane. The transform is set to
                    // the origin with an identity rotation since the mesh for our prefab is updated in Unity World
                    // coordinates.
                    GameObject planeObject = Instantiate(TrackedPlanePrefab, planePosition, Quaternion.identity,
                        transform);
                    planeObject.GetComponent<TrackedPlaneVisualizer>().Initialize(m_NewPlanes[i]);
                }
            }  


            // Disable the snackbar UI when no planes are valid.
            //Session.GetTrackables<TrackedPlane>(m_AllPlanes);
            //bool showSearchingUI = true;
            //for (int i = 0; i < m_AllPlanes.Count; i++)
            //{
            //    if (m_AllPlanes[i].TrackingState == TrackingState.Tracking)
            //    {
            //        showSearchingUI = false;
            //        break;
            //    }
            //}

            //SearchingForPlaneUI.SetActive(showSearchingUI);

            // If the player has not touched the screen, we are done with this update.
            Touch touch;
            if (Input.touchCount < 1 || (touch = Input.GetTouch(0)).phase != TouchPhase.Began)
            {
                return;
            }
            // Ignore touches on UI
            if (EventSystem.current.IsPointerOverGameObject(touch.fingerId)) return;


            // Raycast against the location the player touched to search for planes.
            //TrackableHit hit;
            //TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinBounds | TrackableHitFlags.PlaneWithinPolygon;

            // Instantiate object on first touch on plane
            if (!mainObjectPlaced)
            {
                // Raycast against the location the player touched to search for planes.
                TrackableHit hit;
                TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinBounds | TrackableHitFlags.PlaneWithinPolygon;

                // If touch hits a the raycast filter
                if (Frame.Raycast(touch.position.x, touch.position.y, raycastFilter, out hit))
                {
                    // Place main object
					mainObject = Instantiate(mainObjectPrefab, hit.Pose.position, hit.Pose.rotation);

                    // Create an anchor to allow ARCore to track the hitpoint as understanding of the physical world evolves.
                    var anchor = hit.Trackable.CreateAnchor(hit.Pose);

                    // Andy should look at the camera but still be flush with the plane.
                    if ((hit.Flags & TrackableHitFlags.PlaneWithinPolygon) != TrackableHitFlags.None)
                    {
                        // Get the camera position and match the y-component with the hit position.
                        Vector3 cameraPositionSameY = FirstPersonCamera.transform.position;
                        cameraPositionSameY.y = hit.Pose.position.y;

                        // Have Andy look toward the camera respecting his "up" perspective, which may be from ceiling.
                        mainObject.transform.LookAt(cameraPositionSameY, mainObject.transform.up); 
                    }

                    // Make Andy model a child of the anchor.
                    mainObject.transform.parent = anchor.transform;

                    // Set mainObjectPlaced to true, so we don't instantiate more objects
                    mainObjectPlaced = true;

                    // Disable the rendering of the tracked planes
                    TrackedPlanePrefab.GetComponent<TrackedPlaneVisualizer>().OnTogglePlanes(false);
                }
            }
            // Place and select pinned points once object is placed
            else
            {
                Vector3 clickedPoint;
                Ray ray = Camera.main.ScreenPointToRay(touch.position);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, 1000f))
                {
                    clickedPoint = hit.point;
					publicHit = hit;
                }
                else
                {
                    clickedPoint = Camera.main.ScreenToWorldPoint(touch.position);
                }

                // Insert new clickedPointObject if the center object is touched, and a clickedPointObject 
                // doesn't already exist in this space. Add this object to the clickedPoints list and set 
                // it as the selectedObject
                //if (hit.collider == null) return;
                if (hit.collider.tag == "CenterObject")
                {
                    GameObject placedPoint = Instantiate(clickedPointObject, clickedPoint, Quaternion.identity) as GameObject;
                    clickedPoints.Add(placedPoint);
                    // Andy should look at the camera but still be flush with the plane.
                    //placedPoint.transform.LookAt(FirstPersonCamera.transform);
                    //placedPoint.transform.rotation = Quaternion.Euler(0.0f,
                    //placedPoint.transform.rotation.eulerAngles.y, placedPoint.transform.rotation.z);

                    // Trying to rotate object so it is perpendicular to the point that the user hit - so the pin 
                    // always sticks out from the model.
                    //placedPoint.transform.LookAt(hit2.transform.position + hit2.transform.forward, hit2.normal);
                    //placedPoint.transform.rotation.SetLookRotation(-hit2.normal);

                    selectedObjectInt = clickedPoints.Count - 1;
                    selectedObject = clickedPoints[selectedObjectInt];

					// Make the newly clicked point a child of the mainObject
					clickedPoints[selectedObjectInt].transform.parent = hit.collider.gameObject.transform; //mainObject.transform;


                    // Clear input field when new focal point is placed 
                    inputField.text = "";

                    bottomBar.SetActive(true);
                }
                // If an existing clickedPoint is clicked, set it as the selectedObject.
                else if (hit.collider.tag == "ClickedPoint")
                {
                    Debug.Log("Existing ClickedPoint hit");
                    selectedObject = hit.collider.gameObject;
                    //inputField.ActivateInputField();
                    inputField.text = selectedObject.GetComponentInChildren<Text>().text;

                    bottomBar.SetActive(true);

                }
                else if (hit.collider.tag != "ClickedPoint" && hit.collider.tag != "CenterObject")
                {
                    // DOES NOT WORK :: Deselect clicked point if click is not on the object or an existing clicked point
                    selectedObject.GetComponent<SelectedClickPoint>().SetToDefaultMaterial();
                    selectedObject = null;
                    bottomBar.SetActive(false);
                }
            }

        }
        



        /// <summary>
        /// Quit the application if there was a connection error for the ARCore session.
        /// </summary>
        private void _QuitOnConnectionErrors()
        {
            if (m_IsQuitting)
            {
                return;
            }

            // Quit if ARCore was unable to connect and give Unity some time for the toast to appear.
            if (Session.Status == SessionStatus.ErrorPermissionNotGranted)
            {
                _ShowAndroidToastMessage("Camera permission is needed to run this application.");
                m_IsQuitting = true;
                Invoke("_DoQuit", 0.5f);
            }
            else if (Session.Status.IsError())
            {
                _ShowAndroidToastMessage("ARCore encountered a problem connecting.  Please start the app again.");
                m_IsQuitting = true;
                Invoke("_DoQuit", 0.5f);
            }
        }

        /// <summary>
        /// Actually quit the application.
        /// </summary>
        private void _DoQuit()
        {
            Application.Quit();
        }

        /// <summary>
        /// Show an Android toast message.
        /// </summary>
        /// <param name="message">Message string to show in the toast.</param>
        private void _ShowAndroidToastMessage(string message)
        {
            AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject unityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

            if (unityActivity != null)
            {
                AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
                unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
                {
                    AndroidJavaObject toastObject = toastClass.CallStatic<AndroidJavaObject>("makeText", unityActivity,
                        message, 0);
                    toastObject.Call("show");
                }));
            }
        }
    }
}
