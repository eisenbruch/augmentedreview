﻿using System.Collections;
using System.Collections.Generic;
using GoogleARCore.HelloAR;
using UnityEngine;
using UnityEngine.UI;

public class SubmitText : MonoBehaviour
{

    private GameObject controller;
    private ARController c;

    public void Start()
    {
        controller = GameObject.FindGameObjectWithTag("Controller");
        c = controller.GetComponent<ARController>();
    }

    // Submit text function
    public void Submit_Text()
    {
        c.selectedObject.GetComponentInChildren<Text>().text = c.inputField.text;
        //inputField.text = "";
    }
}
