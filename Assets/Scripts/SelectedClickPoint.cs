﻿using System.Collections;
using System.Collections.Generic;
using GoogleARCore.HelloAR;
using UnityEngine;

public class SelectedClickPoint : MonoBehaviour {

    Renderer rend;
    GameObject controller;
    public SpriteRenderer selectedSprite;
    public SpriteRenderer unselectedSprite;

    public Material selectedMaterial;
    public Material defaultMaterial; 

    void Start()
    {
        rend = GetComponentInChildren<MeshRenderer>();
        controller = GameObject.FindWithTag("Controller");

        //selectedSprite.enabled = true;
        //unselectedSprite.enabled = false;
    }

    void Update () 
    {
        if (controller.GetComponent<ARController>().selectedObject.gameObject == this.gameObject)
        {
            rend.material = selectedMaterial;
            //selectedSprite.enabled = true;
            //unselectedSprite.enabled = false;
        } else {
            rend.material = defaultMaterial;
            //selectedSprite.enabled = false;
            //unselectedSprite.enabled = true;
        }
	}

	private void FixedUpdate()
	{
		// Keep POI the same relative size even if main object is scaled
        Vector3 mainObjectScale = controller.GetComponent<ARController>().mainObject.transform.lossyScale;
        transform.localScale = Vector3.one;
        transform.localScale = new Vector3(mainObjectScale.x / transform.lossyScale.x, 
                                           mainObjectScale.y / transform.lossyScale.y, 
                                           mainObjectScale.z / transform.lossyScale.z);

        //transform.localScale = new Vector3(transform.lossyScale.x / mainObjectScale.x,
                                           //transform.lossyScale.y / mainObjectScale.y,
                                           //transform.lossyScale.z / mainObjectScale.z);
	}

    public void SetToDefaultMaterial(){
        rend.material = defaultMaterial;
    }
}