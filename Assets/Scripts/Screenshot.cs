﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NatShareU;

public class Screenshot : MonoBehaviour {

	public Camera camera2;
	public Image imageOnScreen;
	public GameObject screenshotPanel;

	public Texture2D screenshot;

	public void Start()
	{
		screenshotPanel.SetActive(false);
	}

	public void TakeScreenshot()
	{
		StartCoroutine(CaptureImage());
		screenshotPanel.SetActive(true);
     
	}
    
	public IEnumerator CaptureImage()
    {
		WaitForEndOfFrame frameEnd = new WaitForEndOfFrame();
        yield return frameEnd;

		screenshot = new Texture2D(Screen.width, Screen.height);
        camera2.Render();
        RenderTexture.active = camera2.targetTexture;
		screenshot.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        screenshot.Apply();
		RenderTexture.active = null;

		Sprite sprite = Sprite.Create(screenshot, new Rect(0, 0, screenshot.width, screenshot.height), new Vector2(0.5f, 0.5f));
		imageOnScreen.GetComponent<Image>().sprite = sprite;
        //return captured;
        
    }


	public void ShareScreenshot()
	{
		screenshot.Share();
	}

	public void SaveScreenshot()
    {
		screenshot.SaveToCameraRoll();
    }











    public void CloseScreenshotPanel()
	{
		screenshotPanel.SetActive(false);
	}
}
