﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddMeshColliders : MonoBehaviour 
{
 //   void Start() 
 //   {
 //       StartCoroutine(MyCoroutine());
	//}

    //IEnumerator MyCoroutine()
    //{
    //    Transform[] allChildren = GetComponentsInChildren<Transform>();
    //    foreach (Transform child in allChildren)
    //    {
    //        if (child.gameObject.GetComponent<MeshFilter>())
    //        {
    //            if (!child.gameObject.GetComponent<MeshCollider>())
    //            {
    //                child.gameObject.AddComponent<MeshCollider>();
    //            }
    //        }
    //        child.gameObject.tag = "CenterObject";
    //        yield return null;
    //    }
    //}

    [ContextMenu("Add Mesh Colliders and CenterObject Tag")]
    public void AddCollidersTag()
    {
        Transform[] allChildren = GetComponentsInChildren<Transform>();
        foreach (Transform child in allChildren)
        {
            if (child.gameObject.GetComponent<MeshFilter>())
            {
                if (!child.gameObject.GetComponent<MeshCollider>())
                {
                    child.gameObject.AddComponent<MeshCollider>();
                }
            }
            child.gameObject.tag = "CenterObject";
        }
    }



}
