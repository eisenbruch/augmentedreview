﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleARCore.HelloAR;


public class AudioController : MonoBehaviour {

    private GameObject controller;
    private ARController c;
    private AudioSource aud;

    public void Start()
    {
        controller = GameObject.FindGameObjectWithTag("Controller");
        c = controller.GetComponent<ARController>();
    }


    public void Start_Record_Audio()
    {
        GoogleARCore.AndroidPermissionsManager.RequestPermission("android.permission.RECORD_AUDIO");
        c.selectedObject.GetComponentInChildren<AudioSource>().clip = Microphone.Start(null, false, 99, 44100);
    }

    public void Stop_Record_Audio()
    {
        Microphone.End(null);
    }

    public void Play_Audio()
    {
        c.selectedObject.GetComponentInChildren<AudioSource>().Play();
    }
	
}
