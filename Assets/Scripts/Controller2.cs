﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Controller2 : MonoBehaviour 
{

    public GameObject mainObject;
    public GameObject clickedPointObject;

    public List<GameObject> clickedPoints = new List<GameObject>();

    public int selectedObjectInt; 
    public GameObject selectedObject;

    public Text labelText;
    public InputField inputField;


	// Use this for initialization
	void Start () 
    {
		
	}
	
	// Update is called once per frame
	void Update () 
    {  
        // Ignore touches on UI
        if (EventSystem.current.IsPointerOverGameObject()) return;

        // Set the mouse position
        Vector3 mousePos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0f);

        if (Input.GetMouseButtonDown(0))
        {
            Vector3 clickedPoint;
            Ray ray = Camera.main.ScreenPointToRay(mousePos); 
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 1000f))
            {
                clickedPoint = hit.point;
            }
            else
            {
                clickedPoint = Camera.main.ScreenToWorldPoint(mousePos);
            }

            // Insert new clickedPointObject if the center object is touched, and a clickedPointObject 
            // doesn't already exist in this space. Add this object to the clickedPoints list and set 
            // it as the selectedObject
            if (hit.collider == null) return;
            if (hit.collider.tag == "CenterObject")
            {
                clickedPoints.Add(Instantiate(clickedPointObject, clickedPoint, Quaternion.identity) as GameObject);
                //or for tandom rotarion use Quaternion.LookRotation(Random.insideUnitSphere)
                selectedObjectInt = clickedPoints.Count - 1;
                selectedObject = clickedPoints[selectedObjectInt];

                // Set focus on input field when new focal point is placed 
                inputField.text = "";
                inputField.ActivateInputField();
            }
            // If an existing clickedPoint is clicked, set it as the selectedObject.
            else if (hit.collider.tag == "ClickedPoint")
            {
                Debug.Log("Existing ClickedPoint hit");
                selectedObject = hit.collider.gameObject;
                inputField.ActivateInputField();
                inputField.text = selectedObject.GetComponentInChildren<Text>().text;
                labelText.text = selectedObject.GetComponentInChildren<Text>().text;
            }
        }
	}

    public void Submit_Text(){
        selectedObject.GetComponentInChildren<Text>().text = inputField.text;
        labelText.text = selectedObject.GetComponentInChildren<Text>().text;
        //inputField.text = "";
    }


}
