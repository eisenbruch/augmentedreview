﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean;
using GoogleARCore.HelloAR;


public class ObjectTouchControls : MonoBehaviour {

    ARController c;
    Vector3 objectPosition;

	// Use this for initialization
	void Start () {

        GameObject controller = GameObject.FindGameObjectWithTag("Controller");
        c = controller.GetComponent<ARController>();

        objectPosition = transform.position;

        // Add 2 finger scaling with Lean Touch (MOVE TO NEW SCRIPT)
        gameObject.AddComponent<Lean.Touch.LeanScale>();
        GetComponent<Lean.Touch.LeanScale>().Camera = c.FirstPersonCamera;
        GetComponent<Lean.Touch.LeanScale>().RequiredFingerCount = 2;
        GetComponent<Lean.Touch.LeanScale>().RequiredSelectable =
            GameObject.FindGameObjectWithTag("LeanSelectable").GetComponent<Lean.Touch.LeanSelectable>();

        // Add 3 finger translating with Lean Touch (MOVE TO NEW SCRIPT)
        gameObject.AddComponent<Lean.Touch.LeanTranslate>();
        GetComponent<Lean.Touch.LeanTranslate>().Camera = c.FirstPersonCamera;
        GetComponent<Lean.Touch.LeanTranslate>().RequiredFingerCount = 3;
        GetComponent<Lean.Touch.LeanTranslate>().RequiredSelectable =
            GameObject.FindGameObjectWithTag("LeanSelectable").GetComponent<Lean.Touch.LeanSelectable>();

        // Add 2 finger rotating with Lean Touch (MOVE TO NEW SCRIPT)
        gameObject.AddComponent<Lean.Touch.LeanRotate>();
        GetComponent<Lean.Touch.LeanRotate>().Camera = c.FirstPersonCamera;
        GetComponent<Lean.Touch.LeanRotate>().RequiredFingerCount = 2;
        GetComponent<Lean.Touch.LeanRotate>().RequiredSelectable =
            GameObject.FindGameObjectWithTag("LeanSelectable").GetComponent<Lean.Touch.LeanSelectable>();
        GetComponent<Lean.Touch.LeanRotate>().RotateAxis.x = 0;
        GetComponent<Lean.Touch.LeanRotate>().RotateAxis.y = -1;
        GetComponent<Lean.Touch.LeanRotate>().RotateAxis.z = 0;
		
	}
	
	// Update is called once per frame
	void Update () {
        //Keep Object on floor (NOT WORKING)
        //objectPosition.y = 0;
                                            
	}
}
