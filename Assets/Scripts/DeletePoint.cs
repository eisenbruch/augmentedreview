﻿using System.Collections;
using System.Collections.Generic;
using GoogleARCore.HelloAR;
using UnityEngine;

public class DeletePoint : MonoBehaviour 
{

    private GameObject controller;
    private ARController c;

    public void Start()
    {
        controller = GameObject.FindGameObjectWithTag("Controller");
        c = controller.GetComponent<ARController>();
    } 

    // Delete point function
    public void Delete_Point()
    {
        c.clickedPoints.Remove(c.selectedObject);
        Destroy(c.selectedObject);
        c.inputField.text = "";
    }
}
